//========= Copyright � 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: Combine guard effects
//
// $NoKeywords: $
//=============================================================================
#include "cbase.h"
#include "c_ai_basenpc.h"

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
class C_CombineGuard : public C_AI_BaseNPC
{
	DECLARE_CLASS( C_CombineGuard, C_AI_BaseNPC );
public:
	DECLARE_CLIENTCLASS();

					C_CombineGuard();
	virtual			~C_CombineGuard();

private:
	C_CombineGuard( const C_CombineGuard & );
};

IMPLEMENT_CLIENTCLASS_DT( C_CombineGuard, DT_NPC_CombineGuard, CNPC_CombineGuard )
END_RECV_TABLE()

C_CombineGuard::C_CombineGuard( void )
{
}

//-----------------------------------------------------------------------------
// Purpose: Strider class implementation
//-----------------------------------------------------------------------------
C_CombineGuard::~C_CombineGuard()
{
}