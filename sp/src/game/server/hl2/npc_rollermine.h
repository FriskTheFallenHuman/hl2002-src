//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#ifndef NPC_ROLLERMINE_H
#define NPC_ROLLERMINE_H
#ifdef _WIN32
#pragma once
#endif

#include "ai_basenpc.h"
#include "player_pickup.h"

//-----------------------------------------------------------------------------
// Purpose: This class only implements the IMotionEvent-specific behavior
//			It keeps track of the forces so they can be integrated
//-----------------------------------------------------------------------------
class CRollerController : public IMotionEvent
{
	DECLARE_SIMPLE_DATADESC();

public:
	IMotionEvent::simresult_e Simulate( IPhysicsMotionController *pController, IPhysicsObject *pObject, float deltaTime, Vector &linear, AngularImpulse &angular );

	AngularImpulse	m_vecAngular;
	Vector			m_vecLinear;

	void Off( void ) { m_fIsStopped = true; }
	void On( void ) { m_fIsStopped = false; }

	bool IsOn( void ) { return !m_fIsStopped; }

private:
	bool	m_fIsStopped;
};

//------------------------------------
// Spawnflags
//------------------------------------
#define SF_ROLLERMINE_FRIENDLY		(1 << 16)
#define SF_ROLLERMINE_PROP_COLLISION		(1 << 17)

enum rollingsoundstate_t { ROLL_SOUND_NOT_READY = 0, ROLL_SOUND_OFF, ROLL_SOUND_CLOSED, ROLL_SOUND_OPEN };

//=========================================================
//=========================================================
class CNPC_RollerMine : public CNPCBaseInteractive<CAI_BaseNPC>, public CDefaultPlayerPickupVPhysics
{
	DECLARE_CLASS( CNPC_RollerMine, CNPCBaseInteractive<CAI_BaseNPC> );
	DECLARE_SERVERCLASS();

public:

	CNPC_RollerMine( void ) { m_bTurnedOn = true; m_bUniformSight = false; }
	~CNPC_RollerMine( void );

	void	Spawn( void );
	bool	CreateVPhysics();
	void	RunAI();
	void	StartTask( const Task_t *pTask );
	void	RunTask( const Task_t *pTask );
	void	SpikeTouch( CBaseEntity *pOther );
	void	ShockTouch( CBaseEntity *pOther );
	void	CloseTouch( CBaseEntity *pOther );
	void	EmbedTouch( CBaseEntity *pOther );
	float	GetAttackDamageScale( CBaseEntity *pVictim );
	void	VPhysicsCollision( int index, gamevcollisionevent_t *pEvent );
	void	Precache( void );
	void	OnPhysGunPickup( CBasePlayer *pPhysGunUser, PhysGunPickup_t reason );
	void	OnPhysGunDrop( CBasePlayer *pPhysGunUser, PhysGunDrop_t Reason );
	void	StopLoopingSounds( void );
	void	PrescheduleThink();
	bool	ShouldSavePhysics()	{ return true; }
	void	OnRestore();
	void	Bury( trace_t *tr );
	bool	QuerySeeEntity(CBaseEntity *pSightEnt, bool bOnlyHateOrFearIfNPC = false );

	int		RangeAttack1Conditions ( float flDot, float flDist );
	int		SelectSchedule( void );
	int		TranslateSchedule( int scheduleType );
	int		GetHackedIdleSchedule( void );

	bool	OverrideMove( float flInterval ) { return true; }
	bool	IsValidEnemy( CBaseEntity *pEnemy );
	bool	IsPlayerVehicle( CBaseEntity *pEntity );
	bool	IsShocking() { return gpGlobals->curtime < m_flShockTime ? true : false; }
	void	UpdateRollingSound();
	void	UpdatePingSound();
	void	StopRollingSound();
	void	StopPingSound();
	float	RollingSpeed();
	float	GetStunDelay();
	void	EmbedOnGroundImpact();
	void	UpdateEfficiency( bool bInPVS );
	void	DrawDebugGeometryOverlays();
	Vector EyePosition();

	int		OnTakeDamage( const CTakeDamageInfo &info );
	void	TraceAttack( const CTakeDamageInfo &info, const Vector &vecDir, trace_t *ptr, CDmgAccumulator *pAccumulator );

	Class_T	Classify();

	virtual bool ShouldGoToIdleState();
	virtual	void OnStateChange( NPC_STATE OldState, NPC_STATE NewState );

	// Vehicle interception
	bool	EnemyInVehicle( void );
	float	VehicleHeading( CBaseEntity *pVehicle );

	NPC_STATE SelectIdealState();

	// Vehicle sticking
	void		StickToVehicle( CBaseEntity *pOther );
	void		AnnounceArrivalToOthers( CBaseEntity *pOther );
	void		UnstickFromVehicle( void );
	CBaseEntity *GetVehicleStuckTo( void );
	int			CountRollersOnMyVehicle( CUtlVector<CNPC_RollerMine*> *pRollerList );
	void		InputConstraintBroken( inputdata_t &inputdata );
	void		InputRespondToChirp( inputdata_t &inputdata );
	void		InputRespondToExplodeChirp( inputdata_t &inputdata );
	void		InputJoltVehicle( inputdata_t &inputdata );
	void		InputTurnOn( inputdata_t &inputdata );
	void		InputTurnOff( inputdata_t &inputdata );
	void		InputPowerdown( inputdata_t &inputdata );

	void		PreventUnstickUntil( float flTime ) { m_flPreventUnstickUntil = flTime; }

	virtual unsigned int	PhysicsSolidMaskForEntity( void ) const;

	void		SetRollerSkin( void );

	COutputEvent m_OnPhysGunDrop;
	COutputEvent m_OnPhysGunPickup;

protected:
	DEFINE_CUSTOM_AI;
	DECLARE_DATADESC();

	bool	BecomePhysical();
	void	WakeNeighbors();
	bool	WakeupMine( CAI_BaseNPC *pNPC );

	void	Open( void );
	void	Close( void );
	void	Explode( void );
	void	PreDetonate( void );
	void	Hop( float height );

	void	ShockTarget( CBaseEntity *pOther );

	bool	IsActive() { return m_flActiveTime > gpGlobals->curtime ? false : true; }

	// INPCInteractive Functions
	virtual bool	CanInteractWith( CAI_BaseNPC *pUser ) { return true; }
	virtual	bool	HasBeenInteractedWith()	{ return m_bHackedByAlyx; }
	virtual void	NotifyInteraction( CAI_BaseNPC *pUser );

	CSoundPatch					*m_pRollSound;
	CSoundPatch					*m_pPingSound;

	CRollerController			m_RollerController;
	IPhysicsMotionController	*m_pMotionController;

	float	m_flSeeVehiclesOnlyBeyond;
	float	m_flChargeTime;
	float	m_flGoIdleTime;
	float	m_flShockTime;
	float	m_flForwardSpeed;
	int		m_iSoundEventFlags;
	rollingsoundstate_t m_rollingSoundState;

	CNetworkVar( bool, m_bIsOpen );
	CNetworkVar( float, m_flActiveTime );	//If later than the current time, this will force the mine to be active

	bool	m_bHeld;		//Whether or not the player is holding the mine
	EHANDLE m_hVehicleStuckTo;
	float	m_flPreventUnstickUntil;
	float	m_flNextHop;
	bool	m_bStartBuried;
	bool	m_bBuried;
	bool	m_bIsPrimed;
	bool	m_wakeUp;
	bool	m_bEmbedOnGroundImpact;

public:
	CNetworkVar( bool,	m_bHackedByAlyx );

protected:
	// Constraint used to stick us to a vehicle
	IPhysicsConstraint *m_pConstraint;

	bool	m_bTurnedOn;
	bool	m_bUniformSight;

	CNetworkVar( bool,	m_bPowerDown );
	float	m_flPowerDownTime;
	float	m_flPowerDownDetonateTime;

	static string_t gm_iszDropshipClassname;
};

bool NPC_Rollermine_IsRollermine( CBaseEntity *pEntity );
CBaseEntity *NPC_Rollermine_DropFromPoint( const Vector &originStart, CBaseEntity *pOwner, const char *pszTemplate );

#endif // NPC_ROLLERMINE_H
