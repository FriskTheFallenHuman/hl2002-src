//========= Copyright � 2002, Valve LLC, All rights reserved. ============
//
// Purpose: Odell
//
//=============================================================================

#include "cbase.h"
#include "AI_Schedule.h"
#include "AI_Hull.h"
#include "AI_Navigator.h"
#include "AI_Motor.h"
#include "soundent.h"
#include "game.h"
#include "NPCEvent.h"
#include "EntityList.h"
#include "npc_odell.h"
#include "activitylist.h"
#include "soundflags.h"
#include "ai_goalentity.h"
#include "scripted.h"

//-----------------------------------------------------------------------------
//
// CNPC_Odell
//

LINK_ENTITY_TO_CLASS( npc_odell, CNPC_Odell );

//-----------------------------------------------------------------------------
CNPC_Odell::CNPC_Odell()
{
}

//-----------------------------------------------------------------------------
void CNPC_Odell::Precache( void )
{
	PrecacheModel( "models/odell.mdl" );
	
	BaseClass::Precache();
}
 
//-----------------------------------------------------------------------------
void CNPC_Odell::Spawn( void )
{
	Precache();

	BaseClass::Spawn();

	m_Type = CT_UNIQUE;
	SetModel( "models/odell.mdl" );
}

//-----------------------------------------------------------------------------
Class_T	CNPC_Odell::Classify ( void )
{
	return	CLASS_PLAYER_ALLY_VITAL;
}

