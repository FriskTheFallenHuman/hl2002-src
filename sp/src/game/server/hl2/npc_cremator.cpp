//========= Copyright � 1996-2002, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================
#include "cbase.h"
#include "ai_default.h"
#include "ai_task.h"
#include "ai_schedule.h"
#include "ai_hull.h"
#include "ai_motor.h"
#include "soundent.h"
#include "game.h"
#include "npcevent.h"
#include "entitylist.h"
#include "activitylist.h"
#include "ai_basenpc.h"
#include "IEffects.h"
#include "engine/IEngineSound.h"
#include "EntityFlame.h"
#include "npc_cremator.h"
#include "gib.h"
#include "te_effect_dispatch.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

#define CREMATOR_ATTACH_MUZZLE		0
#define CREMATOR_ATTACH_HEAD		1

#define CREM_ATTN_IDLE	(float) 4.5

ConVar	sk_cremator_health( "sk_cremator_health", "0" );

//=========================================================
// Custom schedules
//=========================================================
enum CrematorSchedules
{
	SCHED_CREMATOR_CHASE = LAST_SHARED_SCHEDULE,
	SCHED_CREMATOR_RANGE_ATTACK1,
	SCHED_CREMATOR_IDLE,
	
};

//=========================================================
// Custom tasks
//=========================================================
enum CrematorTasks
{
	TASK_CREMATOR_ALERT = LAST_SHARED_TASK,
	TASK_CREMATOR_IDLE,
	TASK_CREMATOR_RANGE_ATTACK1,
};

//---------------------------------------------------------
// Save/Restore
//---------------------------------------------------------
BEGIN_DATADESC( CNPC_Cremator )
END_DATADESC()

//-----------------------------------------------------------------------------
// Purpose: Initialize the custom schedules
// Input  :
// Output :
//-----------------------------------------------------------------------------
void CNPC_Cremator::InitCustomSchedules(void) 
{
	INIT_CUSTOM_AI( CNPC_Cremator );

	ADD_CUSTOM_TASK( CNPC_Cremator,	TASK_CREMATOR_ALERT );
	ADD_CUSTOM_TASK( CNPC_Cremator,	TASK_CREMATOR_IDLE );
	ADD_CUSTOM_TASK( CNPC_Cremator,	TASK_CREMATOR_RANGE_ATTACK1 );

	ADD_CUSTOM_SCHEDULE( CNPC_Cremator,	SCHED_CREMATOR_CHASE);
	ADD_CUSTOM_SCHEDULE( CNPC_Cremator,	SCHED_CREMATOR_RANGE_ATTACK1 );
	ADD_CUSTOM_SCHEDULE( CNPC_Cremator,	SCHED_CREMATOR_IDLE);

	AI_LOAD_SCHEDULE( CNPC_Cremator,	SCHED_CREMATOR_CHASE );
	AI_LOAD_SCHEDULE( CNPC_Cremator,	SCHED_CREMATOR_RANGE_ATTACK1 );
	AI_LOAD_SCHEDULE( CNPC_Cremator,	SCHED_CREMATOR_IDLE );
}

LINK_ENTITY_TO_CLASS( npc_cremator, CNPC_Cremator );
IMPLEMENT_CUSTOM_AI( npc_cremator, CNPC_Cremator );

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::Precache( void )
{
	PrecacheModel( "models/cremator.mdl" );
	PrecacheModel( "models/cremator_body.mdl" );
	PrecacheModel( "models/cremator_head.mdl" );

	UTIL_PrecacheOther( "weapon_immolator" );

	PrecacheScriptSound( "NPC_Cremator.Alert" );
	PrecacheScriptSound( "NPC_Cremator.Breathe" );
	PrecacheScriptSound( "NPC_Cremator.Die" );
	PrecacheScriptSound( "NPC_Cremator.SeeEnemy" );
	PrecacheScriptSound( "NPC_Cremator.CloakSwish" );
	PrecacheScriptSound( "NPC_Cremator.LeftFootstep" );
	PrecacheScriptSound( "NPC_Cremator.RightFootstep" );
	
	BaseClass::Precache();
}


//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::Spawn( void )
{
	Precache();

	SetModel( "models/cremator.mdl" );
	SetHullType( HULL_MEDIUM );
	SetHullSizeNormal();

	SetSolid( SOLID_BBOX );
	AddSolidFlags( FSOLID_NOT_STANDABLE );
	SetMoveType( MOVETYPE_STEP );
	SetBloodColor( DONT_BLEED );
	m_iMaxHealth		= sk_cremator_health.GetInt();
	m_iHealth			= m_iMaxHealth;
	m_flFieldOfView		= -0.7f;
	m_NPCState			= NPC_STATE_IDLE;

	CapabilitiesClear();
	CapabilitiesAdd( bits_CAP_MOVE_GROUND );
	CapabilitiesAdd( bits_CAP_INNATE_RANGE_ATTACK1 );
	CapabilitiesAdd( bits_CAP_MOVE_SHOOT );

	NPCInit();
	
	BaseClass::Spawn();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
Class_T	CNPC_Cremator::Classify( void )
{
	return	CLASS_COMBINE;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::IdleSound( void )
{
	EmitSound( "NPC_Cremator.Breathe" );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::AlertSound( void )
{
	EmitSound( "NPC_Cremator.Alert" );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::DeathSound( const CTakeDamageInfo &info )
{
	EmitSound( "NPC_Cremator.Die" );

	StopSound( "NPC_Cremator.Breathe" );
	StopSound( "NPC_Cremator.CloakSwish" );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::FoundEnemySound( void )
{
	EmitSound( "NPC_Cremator.SeeEnemy" );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
float CNPC_Cremator::MaxYawSpeed( void )
{
	float flYS = 0;

	switch ( GetActivity() )
	{
	case	ACT_WALK_HURT:		flYS = 30;	break;
	case	ACT_RUN:			flYS = 90;	break;
	case	ACT_IDLE:			flYS = 90;	break;
	case	ACT_RANGE_ATTACK1:	flYS = 40;	break;
	default:
		flYS = 90;
		break;
	}

	return flYS;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::Event_Killed( const CTakeDamageInfo &info ) 
{
	SetModel( "models/cremator_body.mdl" );
	
	int iBIndex;
	
	// Spawning cremator head
	Vector vHeadPosition = GetAbsOrigin();
	QAngle vHeadAngles = GetAbsAngles();
	iBIndex = LookupBone( "ValveBiped.Bip01_Head" );
	if ( iBIndex != -1 )
		GetBonePosition( iBIndex, vHeadPosition, vHeadAngles );

	CGib *pGib = CREATE_ENTITY( CGib, "gib" );
	if( pGib )
	{
		pGib->Spawn( "models/cremator_head.mdl" );
		pGib->SetBloodColor( DONT_BLEED );
		pGib->SetOwnerEntity( this );
		pGib->m_lifeTime = gpGlobals->curtime + 30.0f;
		pGib->SetAbsOrigin( vHeadPosition );
		pGib->SetAbsAngles( vHeadAngles );
		pGib->SetCollisionGroup( COLLISION_GROUP_INTERACTIVE_DEBRIS );
		pGib->SetMoveType( MOVETYPE_VPHYSICS );

		IPhysicsObject *pPhysicsObject = pGib->VPhysicsGetObject();		
		if( pPhysicsObject )
		{
			Vector vVel	= GetAbsVelocity();
			pPhysicsObject->AddVelocity( &vVel, NULL );
		}
	}
	
	// Spawning weapon_immolator
	Vector vGunPosition = GetAbsOrigin();
	QAngle vGunAngles = GetAbsAngles();
	iBIndex = LookupBone( "ValveBiped.Bip01_R_Hand" );
	if ( iBIndex != -1 )
		GetBonePosition( iBIndex, vGunPosition, vGunAngles );

	CBaseEntity *pImmolator = NULL;
	pImmolator = DropItem( "weapon_immolator", vGunPosition, vGunAngles );
	pImmolator->SetCollisionGroup( COLLISION_GROUP_INTERACTIVE_DEBRIS );
	pImmolator->SetMoveType( MOVETYPE_VPHYSICS );	  
	IPhysicsObject *pImmolatorPhysicsObject = pImmolator->VPhysicsGetObject();
	if ( pImmolatorPhysicsObject )
	{
		Vector vVel	= GetAbsVelocity();
		pImmolatorPhysicsObject->AddVelocity( &vVel, NULL );
	}

	BaseClass::Event_Killed( info );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::FootstepEffect( const Vector &origin )
{
	trace_t tr;
	AI_TraceLine( origin, origin - Vector( 0,0,0 ), MASK_SOLID_BRUSHONLY, this, COLLISION_GROUP_NONE, &tr );
	float yaw = random->RandomInt( 0, 0 );
	for ( int i = 0; i < 2; i++ )
	{
		if ( UTIL_PointContents( tr.endpos + Vector( 0, 0, 1 ) ) & MASK_WATER )
		{
			float flWaterZ = UTIL_FindWaterSurface( tr.endpos, tr.endpos.z, tr.endpos.z + 100.0f );

			CEffectData	data;
			data.m_fFlags = 0;
			data.m_vOrigin = tr.endpos;
			data.m_vOrigin.z = flWaterZ;
			data.m_vNormal = Vector( 0, 0, 1 );
			data.m_flScale = random->RandomFloat( 10.0, 14.0 );

			DispatchEffect( "watersplash", data );
		}
		
		else
		{
			Vector dir = UTIL_YawToVector( yaw + i*180 ) * 10;
			VectorNormalize( dir );
			dir.z = 0.25;
			VectorNormalize( dir );

			g_pEffects->Dust( tr.endpos, dir, 12, 50 );
		}
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
int CNPC_Cremator::OnTakeDamage_Alive( const CTakeDamageInfo &info )
{
	CTakeDamageInfo newInfo = info;
	if( newInfo.GetDamageType() & DMG_BURN )
		newInfo.ScaleDamage( 0 );

	return BaseClass::OnTakeDamage_Alive( newInfo );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::TraceAttack( const CTakeDamageInfo &info, const Vector &vecDir, trace_t *ptr, CDmgAccumulator *pAccumulator )
{	
	BaseClass::TraceAttack( info, vecDir, ptr, pAccumulator );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::Ignite( float flFlameLifetime )
{
	BaseClass::Ignite( flFlameLifetime );

	Activity activity = GetActivity();
	if ( activity == ACT_WALK || activity == ACT_RUN )
		SetActivity( ( Activity )ACT_RUN_HURT );
	else if ( activity == ACT_IDLE )
		SetSequenceByName( "fireinout" );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::StartTask( const Task_t *pTask )
{
	switch ( pTask->iTask )
	{
	case TASK_CREMATOR_IDLE:
		{
			SetActivity( ACT_IDLE );
			TaskComplete();
		}
		break;
	case TASK_CREMATOR_RANGE_ATTACK1:
		{
			Vector flEnemyLKP = GetEnemyLKP();
			GetMotor()->SetIdealYawToTarget( flEnemyLKP );
			SetSequenceByName( "fireinout" );
		}
		break;
	default:
		BaseClass::StartTask( pTask );
		break;
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::RunTask( const Task_t *pTask )
{
	switch ( pTask->iTask )
	{
	case TASK_CREMATOR_RANGE_ATTACK1:
		{
			Vector flEnemyLKP = GetEnemyLKP();
			GetMotor()->SetIdealYawToTargetAndUpdate( flEnemyLKP );
			if ( IsActivityFinished() || IsActivityMovementPhased( ACT_WALK ) || IsActivityMovementPhased( ACT_RUN ) )
			{				
			//	StopParticleEffects( this );
				TaskComplete();
				return;
			}
		}
		break;
	}
	BaseClass::RunTask( pTask );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
Vector CNPC_Cremator::FootHit( float eventtime, bool right )
{	
	Vector footPosition;
	QAngle dummy;
	int iBIndex;
	
	if ( !right )
	{
		iBIndex = LookupBone( "ValveBiped.Bip01_L_Foot" );
		if ( iBIndex != -1 )
			GetBonePosition( iBIndex, footPosition, dummy );

		footPosition.z -= 5.0f;
		CPASAttenuationFilter filter( this );
		EmitSound( filter, entindex(), "NPC_Cremator.LeftFootstep", &footPosition, eventtime );
	}
	else
	{
		iBIndex = LookupBone( "ValveBiped.Bip01_R_Foot" );
		if ( iBIndex != -1 )
			GetBonePosition( iBIndex, footPosition, dummy );

		footPosition.z -= 5.0f;
		CPASAttenuationFilter filter( this );
		EmitSound( filter, entindex(), "NPC_Cremator.RightFootstep", &footPosition, eventtime );
	}

	FootstepEffect( footPosition );
	return footPosition;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CNPC_Cremator::HandleAnimEvent( animevent_t *pEvent )
{
	switch( pEvent->event )
	{
		case NPC_EVENT_LEFTFOOT: // 2050
			{	
				FootHit( pEvent->eventtime, false );
			}
			break;

		case NPC_EVENT_RIGHTFOOT: // 2051
			{				
				FootHit( pEvent->eventtime, true );
			}
			break;
		default:
			BaseClass::HandleAnimEvent( pEvent );
			break;
	}
	BaseClass::HandleAnimEvent( pEvent );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
NPC_STATE CNPC_Cremator::SelectIdealState( void )
{
	switch( m_NPCState )
	{
	case NPC_STATE_COMBAT:
		{
			if ( GetEnemy() == NULL )
			{
				if ( !HasCondition( COND_ENEMY_DEAD ) )
				{
					SetCondition( COND_ENEMY_DEAD );
				}
				return NPC_STATE_ALERT;
			}
			else if ( HasCondition( COND_ENEMY_DEAD ) )
			{
				
			}
		}
	default:
		{
			return BaseClass::SelectIdealState();
		}
	}

	return GetIdealState();;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
Activity CNPC_Cremator::NPC_TranslateActivity( Activity baseAct )
{
	/*if ( baseAct == ACT_WALK || ACT_RUN )
	{
		if ( m_iHealth < ( m_iMaxHealth * 0.5 ) )
			//return (Activity)ACT_WALK_HURT; // the animaton is broken, don't use it
	}*/

	return BaseClass::NPC_TranslateActivity( baseAct );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
int CNPC_Cremator::RangeAttack1Conditions( float flDot, float flDist )
{
	if ( flDist > 256 )
		return COND_TOO_FAR_TO_ATTACK;
	if ( flDot < 0.7f )
		return COND_NOT_FACING_ATTACK;
	return COND_CAN_RANGE_ATTACK1;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
int CNPC_Cremator::SelectSchedule( void )
{
	switch( m_NPCState )
	{
	case NPC_STATE_IDLE:
	case NPC_STATE_ALERT:
		{				
			if ( HasCondition ( COND_HEAR_COMBAT || COND_HEAR_PLAYER || COND_HEAR_BULLET_IMPACT ) )
				return SCHED_INVESTIGATE_SOUND;
		}
		break;
	case NPC_STATE_COMBAT:
		{
			if ( HasCondition( COND_CAN_RANGE_ATTACK1 ) )
			{
				return SCHED_RANGE_ATTACK1;
			}
			if ( HasCondition( COND_SEE_ENEMY ) )
			{
				if( HasCondition( COND_TOO_FAR_TO_ATTACK ) )
				{
					return SCHED_CREMATOR_CHASE;
				}
			}
			if ( HasCondition( COND_TOO_FAR_TO_ATTACK || COND_ENEMY_OCCLUDED ))
			{
				return SCHED_CREMATOR_CHASE;
			}
		}
		break;
	}
	return BaseClass::SelectSchedule();
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
int CNPC_Cremator::TranslateSchedule( int type ) 
{
	switch( type )
	{
	case SCHED_RANGE_ATTACK1:
		return SCHED_CREMATOR_RANGE_ATTACK1;
		break;
	case SCHED_CHASE_ENEMY:
		return SCHED_CREMATOR_CHASE;
		break;
	}
	return BaseClass::TranslateSchedule( type );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
int CNPC_Cremator::GetSoundInterests( void )
{
	return	SOUND_WORLD			|
			SOUND_COMBAT		|
			SOUND_BULLET_IMPACT	|
			SOUND_PLAYER;
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
AI_DEFINE_SCHEDULE
(
	SCHED_CREMATOR_CHASE,

	"	Tasks"
	"		TASK_SET_TOLERANCE_DISTANCE		320"
	//"		TASK_GET_CHASE_PATH_TO_ENEMY	3072"
	"		TASK_RUN_PATH					0"
	"		TASK_WAIT_FOR_MOVEMENT			0"
	"		TASK_FACE_ENEMY				0"
	"	"
	"	Interrupts"
	"		COND_ENEMY_DEAD"
	"		COND_NEW_ENEMY"
	"		COND_CAN_RANGE_ATTACK1"
	"		COND_CAN_RANGE_ATTACK2"
);
AI_DEFINE_SCHEDULE
(
	SCHED_CREMATOR_RANGE_ATTACK1,

	"	Tasks"
	"		TASK_STOP_MOVING			0"
	"		TASK_FACE_ENEMY				0"
	"		TASK_ANNOUNCE_ATTACK		1"
	//"		TASK_CREMATOR_RANGE_ATTACK1	0"
	"		TASK_PLAY_SEQUENCE			ACTIVITY:ACT_RANGE_ATTACK1"
	"	"
	"	Interrupts"
	"		COND_NEW_ENEMY"
	//"		COND_ENEMY_DEAD"
	//"		COND_NO_PRIMARY_AMMO"
	//"		COND_TOO_FAR_TO_ATTACK"
);
AI_DEFINE_SCHEDULE
(
	SCHED_CREMATOR_IDLE,

	"	Tasks"
	//"		TASK_CREMATOR_RANGE_ATTACK1	0"
	"		TASK_PLAY_SEQUENCE			ACTIVITY:ACT_IDLE"
	"	"
	"	Interrupts"
	"		COND_NEW_ENEMY"
);