//========= Copyright (c) 1996-2002, Valve LLC, All rights reserved. ==========
//
// Purpose:
//
// $NoKeywords: $
//=============================================================================

#ifndef	NPC_ODELL_H
#define	NPC_ODELL_H

#include "npc_citizen17.h"

//-----------------------------------------------------------------------------
// CNPC_Odell
//-----------------------------------------------------------------------------
class CNPC_Odell : public CNPC_Citizen
{
	DECLARE_CLASS( CNPC_Odell, CNPC_Citizen );

public:
	CNPC_Odell();

	void Spawn( void );
	void Precache( void );

	Class_T		Classify( void );
};

#endif	//NPC_ODELL_H